#! /bin/bash
#
# Purpose:
# convert a MusicXML to a MEI using XSLT tablesheet

#export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#export JAVA_HOME=`/usr/libexec/java_home -v 1.8`

### CONFIGURATION
XSL="musicxml_clean_verovio-guido.xsl"
USESAXON=0

### INPUTS
XML="${1}"
# ask for MusicXML input if not provided
if [[  ! -f "${XML}" ]]; then
  read -p "Drag'n drop or type the path of a MusicXML file : `echo $'\n> '`" XML
fi

### OUTPUTS
MEI="${XML%.xml}-clean.xml"

### PRE-OPERATION
# move current foler to script folder
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${DIR}"

### MAIN OPERATION
# Apply XSLT stylesheet on XML
echo "Running cleaning operation on MusicXML file..."
if [ $USESAXON == 1 ]; then
  # Remove DTD information (to avoid errors 403)
  XML2=$(mktemp "${TMPDIR}$(uuidgen).xml")
  cp "${XML}" "${XML2}"
  XML="${XML2}"
  sed -i -e 's/http:\/\/www.musicxml.org\/dtds\/partwise.dtd/https:\/\/www.antescofo.com\/Catalog\/dtds\/3.0\/partwise.dtd/g' "${XML}"
  # Call Saxon processor
  java -jar /Users/cuvillier/Documents/Antescofo/Verovio_MusicXML/SaxonPE9-8-0-6J/saxon9pe.jar -xsl:"${XSL}" -s:"${XML}" > "${MEI}"
else
  xsltproc --nodtdattr --novalid --timing -o "${MEI}" "${XSL}" "${XML}"
fi

echo "Done! New MusicXML file is ${MEI}"

### POST-OPERATION
# Open Atom
atom "${MEI}"
