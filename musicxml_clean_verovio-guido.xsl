<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <!--
    XML output, with a DOCTYPE refering the partwise DTD.
    Here we use the full Internet URL.
  -->
  <xsl:output method="xml" indent="yes" encoding="UTF-8"
    omit-xml-declaration="no" standalone="no"
    doctype-system="http://www.musicxml.org/dtds/partwise.dtd"
    doctype-public="-//Recordare//DTD MusicXML 3.1 Partwise//EN" />
  <xsl:strip-space elements="*"/>
  
  <xsl:param name="slurMaxLength" select="1"/>
  
  
  <!--
    Identity template that will copy every
    attribute, element, comment, and processing instruction
    to the output
  -->
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- MAIN OPERATION -->
  <!--
    Step 1. Remove all remove title, other headers, composer, footers, etc.
    In MusicXML all these are represented by <credit> elements
    Rmk. owing to the XSD, <credits> should always be in <score-partwise> or <score-timewise>
    
    Rmk. For now, this operation is USELESS in Verovio and Guido: credits are not displayed
  -->
  <!-- <xsl:template match="credit"/> -->
  
  <!--
    Step 1.bis  #GUIDO-ONLY: For Guido, it is important to remove composer and title, so that they do not appear as headers
  -->
  <xsl:template match="movement-title|creator[@type = 'composer']"/>
  
  
  <!--
    Step 2. Hide instrument short name: <part-abbreviation> and <part-abbreviation-display>
  -->
  <xsl:template match="score-part/part-abbreviation">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="print-object">no</xsl:attribute>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="score-part/part-abbreviation-display">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="print-object">no</xsl:attribute>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
  <!--
    Step 3. Hide instrument full name: <part-name>
  -->
  <xsl:template match="part-name">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="print-object">no</xsl:attribute>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
  
  <!--
    #GUIDO#VEROVIO: remove slurs of type 'start'…
    - without 'stop'-type slur with the name number
    - whose following slur with the same number is of type 'start'
    - whose following slur with the same number is in another voice
    - that is longer than $slurMaxLength measures
  -->
  <xsl:template match="slur[@type='start']">
    <!-- Fetch number of slur -->
    <xsl:variable name="slurNumber" select="@number"/>
    <!-- Fetch voice of slur -->
    <xsl:variable name="voiceNumber" select="preceding::voice[1]/text()"/>
    <!-- Fetch part of slur -->
    <xsl:variable name="partId" select="ancestor::part[1]/@id"/>
    <xsl:choose>
      <!-- <xsl:when test="count(following::slur[@number=$slurNumber])=0"/> -->
      <!-- <xsl:when test="following::slur[@number=$slurNumber][1]/@type='start'"/> -->
      <xsl:when test="following::slur[@type='stop' and @number=$slurNumber and ancestor::part[1]/@id=$partId][1]/preceding::voice[1]/text()!=$voiceNumber"/>
      <xsl:when test="(following::slur[@type='stop' and @number=$slurNumber and ancestor::part[1]/@id=$partId][1]/ancestor::measure[1]/@number) &gt; ancestor::measure[1]/@number + ($slurMaxLength - 1)"/>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

    <!--
    #GUIDO#VEROVIO: remove slurs of type 'stop'…
    - without 'stop'-type slur with the name number
    - whose following slur with the same number is of type 'start'
    - whose preceding 'start' slur with the same number is in another voice
    - that is longer than $slurMaxLength measures
  -->
  <xsl:template match="slur[@type='stop']">
    <!-- Fetch number of slur -->
    <xsl:variable name="slurNumber" select="@number"/>
    <!-- Fetch voice of slur -->
    <xsl:variable name="voiceNumber" select="preceding::voice[1]/text()"/>
    <!-- Fetch part of slur -->
    <xsl:variable name="partId" select="ancestor::part[1]/@id"/>
    <xsl:choose>
      <!-- <xsl:when test="count(following::slur[@number=$slurNumber])=0"/> -->
      <!-- <xsl:when test="following::slur[@number=$slurNumber][1]/@type='start'"/> -->
      <xsl:when test="preceding::slur[@type='start' and @number=$slurNumber and ancestor::part[1]/@id=$partId][1]/preceding::voice[1]/text()!=$voiceNumber"/>
      <xsl:when test="(preceding::slur[@type='start' and @number=$slurNumber and ancestor::part[1]/@id=$partId][1]/ancestor::measure[1]/@number) &lt; ancestor::measure[1]/@number - ($slurMaxLength - 1)"/>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
