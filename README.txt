DONE:

- remove title, other headers, composer, footers, etc.
  <credit page="1"> (in score-header, i.e. in score-partwise and score-timewise)
  for Guido: <creator type="composer"> and <movement-title>
- remove pagination
--> useless
- remove short instrument names
--> add attribute print-object="no" to  <part-abbreviation> and <part-abbreviation-display>
- remove instrument name
--> add attribute print-object="no" to <part-name>
    
- #Guido replace pf dynamics by p - f text
- #Guido replace fp dynamics by f - p text 
- #Guido remove rehearsal marks


- (optional) add ten. text above arppeggios
    > add 
          <direction placement="above">
        <direction-type>
          <words default-y="29" font-style="italic">arp.</words>
        </direction-type>
      </direction>

      over the first note of a chord, i.e. a note which contains
              <notations>
          <slur number="1" type="stop"/>
          <arpeggiate default-x="-12"/>
        </notations>

        but no
        <chord/>



TODO:

- trill wavy-line
    > add
        <notations>
          <tied type="start"/>
          <ornaments>
            <trill-mark default-y="20"/>
          </ornaments>
        </notations>
        on notes betweeen a wavy-line start

            <notations>
          <tied type="start"/>
          <ornaments>
            <trill-mark default-y="20"/>
            <wavy-line default-y="20" number="1" type="start"/>
          </ornaments>
        </notations>

        and a wavy-line stop

        <notations>
          <tied type="start"/>
          <ornaments>
            <wavy-line number="1" type="stop"/>
          </ornaments>
        </notations>

        (and they should be on the same voice as the first note)


- cross-voice slurs 
	> description : remove slurs that starts on some voice and finishes on the other
	> implementation
		- remove

TODO export Verovio:
- remove slurs longer than 2 measures


IMPORT MusicXML-XML
- italic font on Sibelius expression
- italic font
